package edu.cibertec.controller;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.cibertec.model.Empleado;

@Path("api/empleados")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmpleadoResource {

    @Inject
    private EmpleadoController empleadoController;

    @GET
    public List<Empleado> listarEmpleado() {
        return Empleado.listAll();
    }

    @POST
    @Transactional
    public Response agregar(Empleado empleado) {
        Empleado.persist(empleado);
        return Response.ok(empleado).status(201).build();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Response actualizar(@PathParam("id") Long id, Empleado empleado) {
        Empleado emp = empleadoController.update(id, empleado);
        return Response.ok(emp).build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response eliminar(@PathParam("id") Long id) {
        Empleado emp = Empleado.findById(id);
        emp.delete();
        return Response.status(204).build();
    }

    @GET
    @Path("{id}")
    public Empleado listarxId(@PathParam("id") Long id) {
        return Empleado.findById(id);
    }

}