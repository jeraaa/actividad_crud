package edu.cibertec.controller;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import edu.cibertec.model.Departamento;

@ApplicationScoped
public class DepartamentoController {
    public List<Departamento> listar() {
        List<Departamento> listaDepartamento = Departamento.listAll();
        return listaDepartamento;
    }
}
