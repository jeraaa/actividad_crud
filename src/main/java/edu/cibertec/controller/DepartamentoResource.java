package edu.cibertec.controller;

import edu.cibertec.model.Departamento;
import edu.cibertec.model.Empleado;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("api/departamentos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DepartamentoResource {

    @GET
    public List<Departamento> listarDepartamento() {
        return Departamento.listAll();
    }

}
