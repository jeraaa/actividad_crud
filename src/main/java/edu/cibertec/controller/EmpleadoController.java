package edu.cibertec.controller;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import edu.cibertec.model.Empleado;

@ApplicationScoped
public class EmpleadoController {

    public Empleado update(Long id, Empleado empleado) {
        Empleado emp = Empleado.findById(id);

        if (emp == null) {
            throw new WebApplicationException("No existe el empleado con el id " + id, Response.Status.NOT_FOUND);
        }
        emp.setNombre(empleado.getNombre());
        emp.setSalario(empleado.getSalario());
        emp.setFechaIngreso(empleado.getFechaIngreso());

        return emp;
    }
}
