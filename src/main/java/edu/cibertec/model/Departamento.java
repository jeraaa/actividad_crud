package edu.cibertec.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class Departamento extends PanacheEntity {

    @Column(name = "descripcion", length = 100, nullable = false)
    public String descripcion;

    @OneToMany(mappedBy = "departamento", orphanRemoval = true)
    public List<Empleado> empleados = new ArrayList<Empleado>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    public Departamento() {
    }

    public Departamento(Long id, String descripcion, List<Empleado> empleados) {
        this.id = id;
        this.descripcion = descripcion;
        this.empleados = empleados;
    }
}
